kendrick ronel mundiz

Basic Git Commands

ssh-keygen
- is used to generate ssh keys

git config --global user.email "[git account email address]"
- used to configure the global user email

git config --global user.name "[git account username]"
- used to configure the global username

git config --global --list
- used to check the git user credentials

git init
- triggering this command will create '.git' inside of a project folder
- this folder will contain the necessary git information about a project's local repository such as commits and remote git links

git statis 
- triggering this command will display all updates not saved to the lates commit version of the project

git add .
- stages files individually 
- triggering this command will "add" or "stage" the files preparing them to be included in the next commit/snapshot of the project
- adding all files is normal practice to make sure that all files that were updated would be included in the next commit. Individually staging files is useful for minor revisions. 

git commit -m "[message]"
- Triggering this command will create a commit or a snapsho tof the project in the local repository coming from changes added through 
- "Initial Commit" is used in the message to help developers identify the first commit.
- Succeeding commit messages should be descriptive of the changes of the project. (ex. "Added text to discussion.txt file")

git log/ git log --oneling
- Check the commit history

git remote add [remote-name] [git-repository-link]
- triggering this command will add a reference to a git project
- "origin" is mostly used to help developers identify the main remote respository linked to the project
- multiple remote repositories can be added to a project. this would be useful for students when dealing with cloned repositories or for creating multiple repositories for their capstone project. 

git remote -v
- Check the remote names and their corresponding URL
    
git remote remove [remote-name]
- Remove a remote repository
    example: 
	git remote remove origin
git push [remote-name] [branch-name]
- Upload the local repository to a remote repository
    example:
	git push origin master
	git push origin main
git clone [git-repository-link]
- it clones a repository
    example: 
	git clone git@gitlab.com:batch225-nehemiah-ellorico/s02.git


Usual everyday routine

1. git init (inside of a session folder, e.g. sinde s02)
2. got add . (to stage changes)
3. git commit -m "[commit message]"
4. git remote add origin [link]
5. git push origin master